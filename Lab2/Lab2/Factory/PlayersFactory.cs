﻿using ListViewDemoApp.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ListViewDemoApp.Factory
{
    public static class PlayersFactory
    {
        public static IList<Player> Players { get; set; }
        //qq
        static PlayersFactory()
        {
            Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Kobe Bryant",
                    Position = "Shooting guard",
                    Team = "Los Angeles Lakers",
                    Image = "saurezicon.png",
                    Image1 = "saurezfianl.png"
                },
                new Player
                {
                    Name = "LeBron James",
                    Position = "Power forward",
                    Team = "Cleveland Cavaliers",
                    Image = "saurezfianl.png",
                    Image1 = "saurezfianl.png"
                },
                new Player{
                    Name ="Michael Jordan",

                    Position = "Shooting guard",
                    Team ="Chicago Bulls",
                    Image1 = "saurezfianl.png",
                    Image = "saurezfianl.png"
                },
                new Player
                {
                    Name ="Magic Johnson",
                    Position = "Point guard",
                    Team = "Los Angeles Lakers",
                    Image1 = "saurezfianl.png",
                    Image = "saurezfianl.png"
                },
                new Player
                {
                    Name = "Drazen Petrovic",
                    Image1 = "saurezfianl.png",
                    Image = "saurezfianl.png",
                    Position = "Shooting guard",
                    Team = "New Yersey nets"
                },
                
            };
        }
    }
}