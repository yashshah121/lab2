﻿using ListViewDemoApp.Factory;
using ListViewDemoApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab2
{

    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            playerListView.ItemsSource = PlayersFactory.Players;
        }
        private void OnDelete(object sender, System.EventArgs e)
        {
            var listItem = ((MenuItem)sender);
            var player = (Player)listItem.CommandParameter;

            PlayersFactory.Players.Remove(player);
        }

        public void Welcome()
        {
            await DisplayAlert("Alert", "Enter At your own risk", "Ok");
        }
    }
} // Yash is carrying us
